import { useState, useEffect } from 'react'
import { Col, Button, Table } from 'react-bootstrap'
import { Link } from 'react-router-dom'
import request from '../request'
import ReactHTMLTableToExcel from 'react-html-table-to-excel'
import TimeAgo from 'timeago-react'
import './loading.css'

const StockList = () => {
    const [stockList, setStockList] = useState([])
    const [loading, setLoading] = useState(false)
    const [lastTime, setLastTime] = useState('')

    useEffect(() => {
        getAllList()
    }, [])

    const getAllList = async () => {
        try {
            setLoading(true)
            let res = await request.get('/listall')
            setLoading(false)
            setStockList(res.data.data)
            setLastTime(res.data.time.checkTime)
        } catch (error) {
            setLoading(false)
            console.log(error)
        }
    }

    const manualCheck = async () => {
        try {
            setLoading(true)
            let res = await request.get('/check')
            setLoading(false)
            setStockList(res.data.data)
            setLastTime(res.data.time.checkTime)
        } catch (error) {
            setLoading(false)
            console.log(error)
        }
    }

    const deleteCheckStock = async (id) => {
        try {
            setLoading(true)
            await request.delete(`/delete/${id}`)
            setLoading(false)
            getAllList()
        } catch (error) {
            setLoading(false)
            console.log(error)
        }
    }

    return (
        <div>
            {loading && (
                <div className="loading-page">
                    <div>
                        <img
                            src="https://res.cloudinary.com/dan5ie5wm/image/upload/v1616609279/samples/loading_mqn5mz.gif"
                            alt="loading"
                            width="150px"
                            height="150px"
                        />
                    </div>
                </div>
            )}
            <Col className="my-5 text-center">
                <Link
                    to={{
                        pathname: `/addnew/1`,
                        state: { stock: {} },
                    }}
                >
                    {' '}
                    <Button variant="outline-info mr-3">Add New</Button>
                </Link>
                <Button
                    variant="outline-success mr-3"
                    onClick={() => manualCheck()}
                >
                    Manual Check
                </Button>

                <ReactHTMLTableToExcel
                    id="test-table-xls-button"
                    className="btn btn-outline-warning"
                    table="table-to-xls"
                    filename="tablexls"
                    sheet="tablexls"
                    buttonText="Download as XLS"
                />
            </Col>
            <Col className="text-center my-3">
                {lastTime && (
                    <h3>
                        Last Checked Time <TimeAgo datetime={lastTime} />
                    </h3>
                )}
            </Col>
            <div>
                <Table hover id="table-to-xls">
                    <thead>
                        <tr className="bg-secondary text-white">
                            <th>Pic</th>
                            <th>Name</th>
                            <th>Link</th>
                            <th>Check area</th>
                            <th>Stock</th>
                            <th>Status</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        {stockList &&
                            stockList.map((stock) => (
                                <tr key={stock.id}>
                                    <td>
                                        <img
                                            src={stock.pic}
                                            alt="pic"
                                            width="100px"
                                        />
                                    </td>
                                    <td>{stock.name}</td>
                                    <td>{stock.link}</td>
                                    <td>{stock.checkStock}</td>
                                    <td>{stock.stock}</td>
                                    <td>
                                        {stock.different === 1 ? (
                                            <img
                                                src="https://res.cloudinary.com/dvbvpmjez/image/upload/v1619294901/School_Pictures/error_iejyqz.png"
                                                width="24px"
                                                alt="different"
                                            />
                                        ) : (
                                            ''
                                        )}
                                    </td>
                                    <td>
                                        <div>
                                            <Link
                                                to={{
                                                    pathname: `/addnew/2`,
                                                    state: { stock },
                                                }}
                                            >
                                                <Button
                                                    size="sm"
                                                    variant="outline-info"
                                                    style={{ width: '60px' }}
                                                >
                                                    Edit
                                                </Button>
                                            </Link>
                                        </div>
                                        <div className="mt-3">
                                            <Button
                                                size="sm"
                                                variant="outline-danger"
                                                onClick={() => {
                                                    if (
                                                        window.confirm(
                                                            'Are you sure?'
                                                        )
                                                    ) {
                                                        deleteCheckStock(
                                                            stock.id
                                                        )
                                                    }
                                                }}
                                            >
                                                Delete
                                            </Button>
                                        </div>
                                    </td>
                                </tr>
                            ))}
                    </tbody>
                </Table>
            </div>
        </div>
    )
}

export default StockList
