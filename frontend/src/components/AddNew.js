import { useState, useEffect } from 'react'
import { Col, Form, Button, Container } from 'react-bootstrap'
import request from '../request'
import { useHistory } from 'react-router-dom'

const InputContent = ({ name, value, changeFn, label, type }) => {
    return (
        <Col className="my-3">
            <label>{label}</label>
            <input
                className="form-control"
                type={type}
                name={name}
                value={value}
                placeholder={label}
                onChange={(e) => changeFn(e.target.value)}
            />
        </Col>
    )
}

const AddNew = ({ match, location }) => {
    const history = useHistory()
    const type = match.params.type
    const [pic, setPic] = useState('')
    const [name, setName] = useState('')
    const [link, setLink] = useState('')
    const [checkStock, setCheckStock] = useState('')
    const [stock, setStock] = useState(0)

    useEffect(() => {
        if (type === '2') {
            const { pic, name, link, checkStock, stock } = location.state.stock
            setPic(pic)
            setName(name)
            setLink(link)
            setCheckStock(checkStock)
            setStock(stock)
        }
    }, [type, location.state.stock])

    const handleSubmit = async (e) => {
        e.preventDefault()
        if (pic && name && link && checkStock) {
            const values = {
                pic,
                name,
                link,
                checkStock,
                stock,
            }

            try {
                if (type === '2') {
                    await request.put(
                        `update/${location.state.stock.id}`,
                        values
                    )
                } else {
                    await request.post('addnew', values)
                }
                history.push('/')
            } catch (error) {
                console.log(error)
            }
        } else {
            alert('Missing part')
            return false
        }
    }

    return (
        <Container className="my-5">
            <Form onSubmit={handleSubmit}>
                <InputContent
                    name="pic"
                    value={pic}
                    changeFn={setPic}
                    label="Pic"
                    type="text"
                />
                <InputContent
                    name="name"
                    value={name}
                    changeFn={setName}
                    label="Name"
                    type="text"
                />
                <InputContent
                    name="link"
                    value={link}
                    changeFn={setLink}
                    label="Link"
                    type="text"
                />
                <InputContent
                    name="checkStock"
                    value={checkStock}
                    changeFn={setCheckStock}
                    label="Check Stock Arrtibute"
                    type="text"
                />
                <InputContent
                    name="stock"
                    value={stock}
                    changeFn={setStock}
                    label="Stock"
                    type="number"
                />
                <Button type="submit" variant="outline-success">
                    Submit
                </Button>

                <Button
                    variant="outline-info ml-3"
                    onClick={() => history.goBack()}
                >
                    Back
                </Button>
            </Form>
        </Container>
    )
}

export default AddNew
