import { Container } from 'react-bootstrap'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import AddNew from './components/AddNew'
import StockList from './components/StockList'
import Default from './Default'

function App() {
    return (
        <Container>
            <Router>
                <Switch>
                    <Route path="/" exact component={StockList} />
                    <Route path="/addnew/:type" component={AddNew} />
                    <Route component={Default} />
                </Switch>
            </Router>
        </Container>
    )
}

export default App
