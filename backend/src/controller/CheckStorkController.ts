import { Request, Response } from 'express'
import { getRepository } from 'typeorm'
import CheckList from '../entity/CheckList'
import CheckTime from '../entity/CheckTime'

export default class CheckStockController {
    private static get checkListRepo() {
        return getRepository(CheckList)
    }

    private static get checkTimeRepo() {
        return getRepository(CheckTime)
    }

    static listAll = async (req: Request, res: Response) => {
        let checkLists: CheckList[] = null
        let checkTime: CheckTime[] = null
        try {
            checkLists = await CheckStockController.checkListRepo.find({
                order: {
                    updatedAt: 'DESC',
                },
            })
            checkTime = await CheckStockController.checkTimeRepo.find({
                order: { checkTime: 'DESC' },
                take: 1,
            })
            return res
                .status(200)
                .send({ data: checkLists, time: checkTime[0] })
        } catch (error) {
            return res.status(400).send(error.message)
        }
    }

    static addNew = async (req: Request, res: Response) => {
        const { pic, name, link, checkStock, stock } = req.body
        let checkList = new CheckList()
        checkList.pic = pic
        checkList.name = name
        checkList.link = link
        checkList.checkStock = checkStock
        checkList.stock = stock
        try {
            checkList = await CheckStockController.checkListRepo.save(checkList)
            return res.status(201).send({ data: checkList })
        } catch (error) {
            return res.status(400).send(error.message)
        }
    }

    static update = async (req: Request, res: Response) => {
        const id = req.params.id
        const { pic, name, link, checkStock, stock } = req.body
        let checkList: CheckList = null
        try {
            checkList = await CheckStockController.checkListRepo.findOneOrFail(
                id
            )
        } catch (error) {
            return res.status(400).send(error.message)
        }
        checkList.pic = pic
        checkList.name = name
        checkList.link = link
        checkList.checkStock = checkStock
        checkList.stock = stock
        try {
            checkList = await CheckStockController.checkListRepo.save(checkList)
            return res.status(201).send({ data: checkList })
        } catch (error) {
            return res.status(400).send(error.message)
        }
    }

    static delete = async (req: Request, res: Response) => {
        const id = req.params.id
        try {
            await CheckStockController.checkListRepo.findOneOrFail(id)
        } catch (error) {
            return res.status(400).send(error.message)
        }

        try {
            await CheckStockController.checkListRepo.delete(id)
            return res.status(204).send('Delete Successful')
        } catch (error) {
            return res.status(400).send(error.message)
        }
    }
}
