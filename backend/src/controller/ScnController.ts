import { Request, Response } from 'express'
import { getRepository } from 'typeorm'
import CheckList from '../entity/CheckList'
import CheckTime from '../entity/CheckTime'
import { sendEmail } from '../service/sendEmail'
// const nightmare = require('nightmare')()
const Nightmare = require('nightmare')

export class ScnController {
    static checkPrice = async (req: Request, res: Response) => {
        let checkLists: CheckList[] = null
        try {
            checkLists = await getRepository(CheckList).find({
                order: {
                    updatedAt: 'DESC',
                },
            })
        } catch (error) {
            return res.status(400).send(error.message)
        }

        let emailMessage = ''

        console.log(
            `======================= Checking start at ${new Date()} (${
                checkLists.length
            }) =======================`
        )

        for (let i = 0; i < checkLists.length; i++) {
            console.log(
                `check (${i + 1} / ${checkLists.length}) Item`,
                checkLists[i].name
            )
            let stockCheck
            const urlLink = checkLists[i].link
            const urlStock = checkLists[i].checkStock

            try {
                stockCheck = await ScnController.stockFn(urlLink, urlStock)
            } catch (error) {
                console.log(error)
                return res.status(400).send({ error })
            }
            const num = parseInt(stockCheck.split(' ')[0])
            const preNum = checkLists[i].stock
            console.log('get the stockcheck ==>>', stockCheck)

            try {
                if (num === preNum) {
                    checkLists[i].different = 0
                } else {
                    checkLists[i].stock = num
                    checkLists[i].different = 1
                    emailMessage += `

${
    checkLists[i].name
} Stock changed from ${preNum} to ${num} ${new Date().toLocaleDateString()}

                    `
                }
            } catch (error) {
                console.log(error)
                return res.status(400).send({ error })
            }
        }
        try {
            console.log(
                `======================= Checking finished =======================`
            )

            let checkTime = new CheckTime(new Date())
            await getRepository(CheckTime).save(checkTime)

            await getRepository(CheckList).save(checkLists)
            if (emailMessage.length > 0) {
                sendEmail(emailMessage)
            }

            return res.status(200).send({ data: checkLists, time: checkTime })
        } catch (error) {
            return res.status(400).send(error.message)
        }
    }

    static stockFn = async (url, stock) => {
        let stockCheck = null
        try {
            const nightmare = new Nightmare({ show: false })
            stockCheck = nightmare
                .goto('https://www.scnindustrial.com/account/logout')
                .wait('#username')
                // .screenshot(`Logout${index}.jpg`)
                .type('#username', 'jamesc@cangardcare.com')
                .type('#password', 'james100\r\n')
                .wait(2000)
                .goto(url)
                .wait(`#${stock}`)
                .evaluate((stock) => {
                    return document.getElementById(stock).innerText
                }, stock)
                .end()
        } catch (error) {
            throw error
        }
        return stockCheck
    }
}
