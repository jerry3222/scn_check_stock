import { Router } from 'express'
import CheckStockController from '../controller/CheckStorkController'
import { ScnController } from '../controller/ScnController'

const router = Router()

router.get('/check', ScnController.checkPrice)

router.get('/listall', CheckStockController.listAll)

router.post('/addnew', CheckStockController.addNew)

router.put('/update/:id', CheckStockController.update)

router.delete('/delete/:id', CheckStockController.delete)

export default router
