import 'reflect-metadata'
import { createConnection } from 'typeorm'
import * as express from 'express'
import * as bodyParser from 'body-parser'
import * as cors from 'cors'
import * as helmet from 'helmet'
import router from './routes/index'

const HOSTPORT = 3458

createConnection()
    .then(async (connection) => {
        // create express app
        const app = express()

        // Call middlewares
        app.use(cors())
        app.use(helmet())
        app.use(bodyParser.json())

        // configure routes
        app.use('/', router)

        app.listen(HOSTPORT)

        console.log(
            `Express server has started on port ${HOSTPORT}. Open http://localhost:${HOSTPORT}/ to see results`
        )
    })
    .catch((error) => console.log(error))
