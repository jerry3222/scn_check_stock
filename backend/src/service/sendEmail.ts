import { transporter } from '../service/nodemailer'

export const sendEmail = (message) => {
    return transporter.sendMail({
        from: 'jamescao68@gmail.com',
        to: `scninventory@cangardcare.com`,
        subject: `SCN Stock changed`,
        text: `${message}`,
    })
}
