import {
    Column,
    Entity,
    PrimaryGeneratedColumn,
    UpdateDateColumn,
} from 'typeorm'

@Entity()
export default class CheckList {
    @PrimaryGeneratedColumn()
    id: number

    @Column()
    @UpdateDateColumn()
    updatedAt: Date

    @Column({ length: 255 })
    pic: string

    @Column({ length: 255 })
    name: string

    @Column({ length: 255 })
    link: string

    @Column({ length: 50 })
    checkStock: string

    @Column('int')
    stock: number

    @Column('tinyint', { default: 0 })
    different: number
}
