import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm'

@Entity()
export default class CheckTime {
    @PrimaryGeneratedColumn()
    id: number

    @Column('datetime')
    checkTime: Date

    constructor(checkTime: Date) {
        this.checkTime = checkTime
    }
}
